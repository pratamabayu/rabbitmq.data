﻿using System;
using System.Threading;
using RabbitMQ.Data.Models;
using Hosting.Management;
using Hosting.Management.Scheduling;
using System.Threading.Tasks;

namespace RabbitMQ.Data.Example.Tasks
{
	public class MessageScheduledTask : IScheduledTask
    {
		/// <summary>
        /// Gets the schedule. Repeat minutely
        /// </summary>
        /// <value>The schedule.</value>
        public string Schedule => "* * * * *";

		public void Invoke(CancellationToken cancellationToken,
		                   IBus bus,
		                   IMessageEventGroup group)
		{         
			var currentCycledDateTime = DateTime.UtcNow;
            
			for (var i = 0; i < 10; i++)
			{
				try
				{
					bus.Produce(group,
								new MessageEventModel
								{
									ProducedOn = currentCycledDateTime,
									Id = i,
									Caption = $"Message: {i}"
								});

					Console.WriteLine($"Send message: {i}");
				}
				catch
				{
					Console.WriteLine($"Failed to send message: {i}");

					continue;
				}
			}
		}
    }
}
