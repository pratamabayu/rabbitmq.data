﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using RabbitMQ.Data.Models;

namespace RabbitMQ.Data.Example.Controllers
{
    [Route("[controller]")]
    public class MessagesController : Controller
    {
		IBus bus;
		IMessageEventGroup group;

		public MessagesController(IBus bus, IMessageEventGroup group)
		{
			this.bus = bus;
			this.group = group;
		}

        // POST api/values
        [HttpPost]
		public IActionResult Post([FromBody] MessagePayload payload)
        {
			try
			{
				bus.Produce(group, new MessageEventModel
				{
					Id = payload.Id,
					Caption = payload.Caption
				});
			}
			catch
			{
				return BadRequest();
			}

			return Ok();
        }
    }
}
