FROM microsoft/aspnetcore:2.0 AS base
WORKDIR /app
EXPOSE 80

FROM microsoft/aspnetcore-build:2.0 AS build
WORKDIR /src
COPY RabbitMQ.Data.sln ./
COPY RabbitMQ.Data.Example/RabbitMQ.Data.Example.csproj RabbitMQ.Data.Example/
COPY RabbitMQ.Data/RabbitMQ.Data.csproj RabbitMQ.Data/
COPY ../Hosting.Management/Hosting.Management/Hosting.Management.csproj ../Hosting.Management/Hosting.Management/
COPY RabbitMQ.Data.Models/RabbitMQ.Data.Models.csproj RabbitMQ.Data.Models/
RUN dotnet restore -nowarn:msb3202,nu1503
COPY . .
WORKDIR /src/RabbitMQ.Data.Example
RUN dotnet build -c Release -o /app

FROM build AS publish
RUN dotnet publish -c Release -o /app

FROM base AS final
WORKDIR /app
COPY --from=publish /app .
ENTRYPOINT ["dotnet", "RabbitMQ.Data.Example.dll"]
