﻿namespace RabbitMQ.Data
{
    public interface IEventGroup
    {
        string QueueName { get; }

        string ExchangeName { get; }

        string ExchangeType { get; }

        string RoutingKey { get; }
    }
}
