﻿using System;
namespace RabbitMQ.Data
{
	public interface IBus : IDisposable
    {
		void Produce<G, M>(G group, M model) where G : IEventGroup where M : IEventModel;

		M Consume<G, M>(G group) where G : IEventGroup where M : IEventModel;
    }
}
