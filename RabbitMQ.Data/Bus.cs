﻿using System;
using System.Net.Sockets;
using System.Text;
using Newtonsoft.Json;
using Polly;
using RabbitMQ.Client;
using RabbitMQ.Client.Events;
using RabbitMQ.Client.Exceptions;

namespace RabbitMQ.Data
{
    public class Bus : IBus
    {
        object syncRoot = new object();
        bool disposed;

        ConnectionFactory connectionFactory;
        IConnection connection;

        public bool IsConnected
        {
            get
            {
                return connection != null && connection.IsOpen && !disposed;
            }
        }

        public Bus(string hostName, string userName, string password)
        {
            if (string.IsNullOrEmpty(hostName) ||
                string.IsNullOrEmpty(userName) ||
                string.IsNullOrEmpty(password))
                throw new ArgumentException("Constructor parameters couldn't be null");

            connectionFactory = new ConnectionFactory
            {
                HostName = hostName,
                UserName = userName,
                Password = password
            };
        }

        public void Produce<G, M>(G group, M model)
            where G : IEventGroup
            where M : IEventModel
        {
            if (!IsConnected)
            {
                TryConnect();
            }

            var policy = Policy.Handle<BrokerUnreachableException>()
                .Or<SocketException>()
                .WaitAndRetry(5, retryAttempt => TimeSpan.FromSeconds(Math.Pow(2, retryAttempt)), (exception, time) =>
                {
                    Console.WriteLine(exception.Message);
                });

            using (var channel = connection.CreateModel())
            {
                channel.ExchangeDeclare(group.ExchangeName, group.ExchangeType);

                channel.QueueDeclare(group.QueueName,
                                     true,
                                     false,
                                     false,
                                     null);

                channel.QueueBind(group.QueueName,
                                  group.ExchangeName,
                                  group.RoutingKey);

                string data = JsonConvert.SerializeObject(model);

                var body = Encoding.UTF8.GetBytes(data);

                policy.Execute(() =>
                {
                    var properties = channel.CreateBasicProperties();
                    properties.Persistent = true;

                    channel.BasicPublish(group.ExchangeName,
                                         group.RoutingKey,
                                         properties,
                                         body);
                });
            }
        }

        public M Consume<G, M>(G group)
            where G : IEventGroup
            where M : IEventModel
        {
            if (!IsConnected)
            {
                TryConnect();
            }

            var model = default(M);

            using (var channel = connection.CreateModel())
            {
                channel.ExchangeDeclare(group.ExchangeName, group.ExchangeType);

                channel.QueueDeclare(group.QueueName,
                                     true,
                                     false,
                                     false,
                                     null);

                channel.BasicQos(0, 1, false);

                channel.QueueBind(group.QueueName,
                                  group.ExchangeName,
                                  group.RoutingKey);

                var consumer = new EventingBasicConsumer(channel);

                BasicGetResult result = channel.BasicGet(group.QueueName, true);
                if (result != null)
                {
                    string data = Encoding.UTF8.GetString(result.Body);

                    model = JsonConvert.DeserializeObject<M>(data);
                }

                channel.BasicConsume(group.QueueName, false, consumer);
            }

            return model;
        }

        public void Dispose()
        {
            if (disposed) return;

            disposed = true;

            try
            {
                connection.Dispose();
            }
            catch (Exception exception)
            {
                Console.WriteLine(exception.Message);
            }
        }

        bool TryConnect()
        {
            Console.WriteLine("RabbitMQ Client is trying to connect");

            lock (syncRoot)
            {
                var policy = Policy.Handle<SocketException>()
                    .Or<BrokerUnreachableException>()
                    .WaitAndRetry(5, retryAttempt => TimeSpan.FromSeconds(Math.Pow(2, retryAttempt)), (exception, time) =>
                    {
                        Console.WriteLine(exception.Message);
                    }
                );

                policy.Execute(() =>
                {
                    connection = connectionFactory.CreateConnection();
                });

                if (IsConnected)
                {
                    connection.ConnectionShutdown += OnConnectionShutdown;
                    connection.CallbackException += OnCallbackException;
                    connection.ConnectionBlocked += OnConnectionBlocked;

                    Console.WriteLine($"RabbitMQ persistent connection acquired a connection {connection.Endpoint.HostName} and is subscribed to failure events");

                    return true;
                }
                else
                {
                    Console.WriteLine("FATAL ERROR: RabbitMQ connections could not be created and opened");

                    return false;
                }
            }
        }

        void OnConnectionBlocked(object sender, ConnectionBlockedEventArgs e)
        {
            if (disposed) return;

            Console.WriteLine("A RabbitMQ connection is shutdown. Trying to re-connect...");

            TryConnect();
        }

        void OnCallbackException(object sender, CallbackExceptionEventArgs e)
        {
            if (disposed) return;

            Console.WriteLine("A RabbitMQ connection throw exception. Trying to re-connect...");

            TryConnect();
        }

        void OnConnectionShutdown(object sender, ShutdownEventArgs reason)
        {
            if (disposed) return;

            Console.WriteLine("A RabbitMQ connection is on shutdown. Trying to re-connect...");

            TryConnect();
        }
    }
}
