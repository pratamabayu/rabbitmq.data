﻿using System;
using Newtonsoft.Json;

namespace RabbitMQ.Data
{
	public abstract class EventModel : IEventModel
	{
		[JsonProperty("producedOn")]
		public DateTime ProducedOn { get; set; }
	}
}
