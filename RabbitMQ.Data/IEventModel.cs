﻿using System;
using Newtonsoft.Json;

namespace RabbitMQ.Data
{
    public interface IEventModel
    {
		[JsonProperty("producedOn")]
		DateTime ProducedOn { get; set; }
    }
}