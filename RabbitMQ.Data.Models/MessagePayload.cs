﻿using System;
using Newtonsoft.Json;

namespace RabbitMQ.Data.Models
{
    public class MessagePayload
    {
		[JsonProperty("id")]
        public int Id { get; set; }

        [JsonProperty("caption")]
        public string Caption { get; set; }
    }
}
