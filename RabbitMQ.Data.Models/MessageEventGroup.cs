﻿using System;
namespace RabbitMQ.Data.Models
{
	public class MessageEventGroup : IMessageEventGroup
    {
		public string QueueName => "message_queue";

        public string ExchangeName => "MessageExch";

		public string ExchangeType => "direct";

        public string RoutingKey => "message_queue";
    }
}
