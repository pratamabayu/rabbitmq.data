﻿using System;
using System.Diagnostics.Contracts;
using System.Linq;
using System.Reflection;
using System.Threading;
using System.Threading.Tasks;
using Hosting.Management;
using Microsoft.Extensions.DependencyInjection;

namespace RabbitMQ.Data.Worker.Tasks
{
	public abstract class ConsumerTask<G, M> : HostedService where G : IEventGroup where M : IEventModel
    {
		IBus bus;
        G group;

		readonly IServiceScopeFactory serviceScopeFactory;

		public ConsumerTask(IServiceScopeFactory serviceScopeFactory, IBus bus, G group)

        {
			this.serviceScopeFactory = serviceScopeFactory ?? 
				throw new ArgumentNullException(nameof(serviceScopeFactory));

			this.bus = bus;
			this.group = group;
        }

		protected sealed override async Task ExecuteAsync(CancellationToken cancellationToken)
        {
			while (!cancellationToken.IsCancellationRequested)
            {            
				try
				{
					var model = bus.Consume<G, M>(group);

					if (model == null)
						continue;

					using (var scope = serviceScopeFactory.CreateScope())
					{
						var type = this.GetType();

						var method = type.GetMethod("Invoke", BindingFlags.Instance | BindingFlags.Public);
						var arguments = method.GetParameters()
											  .Select(a => a.ParameterType == typeof(M) ? model : scope.ServiceProvider.GetService(a.ParameterType))
											  .ToArray();

						if (typeof(Task).Equals(method.ReturnType))
						{
							await (Task)method.Invoke(this, arguments);
						}
						else
						{
							method.Invoke(this, arguments);
						}
					}
				}
				catch
				{
					Console.WriteLine("Failed to receive message");

					continue;
				}
            }
        }
    }
}
