﻿using System;
using System.Threading.Tasks;
using Microsoft.Extensions.DependencyInjection;
using RabbitMQ.Data.Models;

namespace RabbitMQ.Data.Worker.Tasks
{
	public class MessageConsumerTask : ConsumerTask<IMessageEventGroup, MessageEventModel>
    {
		public MessageConsumerTask(IServiceScopeFactory serviceScopeFactory, IBus bus, IMessageEventGroup group)
			: base(serviceScopeFactory, bus, group)
        {
        }

		public async Task Invoke(MessageEventModel model)
		{
			Console.WriteLine($"Receive message: {model.Id}");

			await Task.Delay(1);
		}
    }
}
