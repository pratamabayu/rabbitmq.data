﻿using System;
using System.Threading.Tasks;
using Microsoft.Extensions.FileProviders;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.DependencyInjection;
using RabbitMQ.Data.Models;
using RabbitMQ.Data.Worker.Tasks;

namespace RabbitMQ.Data.Worker
{
    class Program
    {
		public static async Task Main(string[] args)
        {
            var builder = new HostBuilder()
                .ConfigureHostConfiguration((config) =>
                {
                })
                .ConfigureAppConfiguration((hostContext, config) =>
                {
				    config.SetBasePath(Environment.CurrentDirectory);
                    config.AddJsonFile("appsettings.json", false);
                    config.AddJsonFile($"appsettings.{hostContext.HostingEnvironment.EnvironmentName}.json", true);
                })
                .ConfigureServices((hostContext, services) =>
                {            
				    services.AddSingleton<IMessageEventGroup, MessageEventGroup>();

                    services.AddSingleton<IBus>(new Bus("RabbitMQDataHost", "guest", "guest"));
    
                    services.AddSingleton<IHostedService, MessageConsumerTask>();
                });

            await builder.RunConsoleAsync();
        }
    }
}
